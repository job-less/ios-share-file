const path = require('path')
const config = {};
// GLOBAL
config.root = path.normalize(`${__dirname}`);
config.url = process.env.URL || 'http://localhost';
config.port = process.env.PORT || 3000;
// BODY PARSER
config.bodyParser = {}
config.bodyParser.limit = process.env.LIMITE || '100mb'
config.bodyParser.contentType = process.env.CONTENTTYPE || '*/*';
// FOLDERS
config.folder = {}
config.folder.destination = path.normalize(process.env.DESTINATION || 'download')

module.exports = config;
