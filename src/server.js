const express = require('express'),
	app = express(),
	bodyParser = require('body-parser'),
	fs = require('fs'),
	convert = require('heic-convert'),
	config = require(`${__dirname}/config`);

app
	.use(bodyParser.raw({
		limit: config.bodyParser.limit,
		type: config.bodyParser.contentType
	}))
	.post('/', async (req, res) => {
		const file = {
			name: req.headers['file-name'],
			ext: req.headers['file-ext'],
			data: req.body
		}
		console.log(file)
		switch (file.ext) {
			case 'heic':
				convertHEICtoPNG(file);
				break;
			default:
				saveBuffer(file);
		}
		res.json({ status: 'ok' })
	})
	.listen(3000)
	
const convertHEICtoPNG = async (file) => {
	file.data = await convert({
		buffer: file.data,
		format: 'PNG',
	});
  file.ext = 'png';
  await saveBuffer(file)
};

const saveBuffer = async (file) => {
	fs.writeFile(`${config.folder.destination}/${file.name}.${file.ext}`, file.data,(err) => {
	  if(err) throw new Error(err);
	  console.log("The file was saved!");
	});
}
